import {
  shallowRef
} from 'vue'

export const createDialogItem = (component, props, id) => ({
  component: shallowRef(component),
  id: id != null ? id : Date.now(),
  props: Object.assign({}, props, {
    modelValue: true
  }),
  onClose: null
})

export const createDialogMethods = () => {
  const dialogs = shallowRef([])

  const addDialog = (params, hooks) => {
    const item = createDialogItem(params.component, params.props, params.id)

    if (hooks != null && hooks.onClose != null) {
      item.onClose = hooks.onClose
    }

    dialogs.value.push(item)

    return item.id
  }

  const removeDialog = (id, closeDelay = 0) => {
    const index = dialogs.value.findIndex(item => item.id === id)

    if (index >= 0) {
      if (closeDelay > 0) {
        setTimeout(() => {
          dialogs.value.splice(index, 1)
        }, closeDelay)
      } else {
        dialogs.value.splice(index, 1)
      }
    }
  }

  return {
    dialogs,
    addDialog,
    removeDialog
  }
}

export const plugin = {
  install(app) {
    const methods = createDialogMethods()

    app.provide('dialogMethods', methods)
    app.config.globalProperties.$dialog = methods
  }
}

export const dialogInjectionKey = Symbol('dialogMethods')