export function convertToUnit(str, unit = 'px') {
  if (str == null || str === '')
    return undefined
  else if (isNaN(+str))
    return String(str)
  else if (!isFinite(+str))
    return undefined
  else
    return `${Number(str)}${unit}`;
}
