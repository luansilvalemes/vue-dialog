const hasScrollbar = (el) => {
  if (!el || el.nodeType !== Node.ELEMENT_NODE) return false;

  const style = window.getComputedStyle(el);
  return ["auto", "scroll"].includes(style.overflowY) && el.scrollHeight > el.clientHeight;
};

const shouldScroll = (el, delta) => {
  if (el.scrollTop === 0 && delta < 0) return true;
  return el.scrollTop + el.clientHeight === el.scrollHeight && delta > 0;
};

export const noScrollableParent = (event, content) => {
  const path = event.composedPath();
  const delta = event.deltaY;

  for (let index = 0; index < path.length; index++) {
    const el = path[index];

    if (el === document) return true;
    if (el === document.documentElement) return true;
    if (el === content) return true;

    if (hasScrollbar(el)) return shouldScroll(el, delta);
  }

  return true;
};

export const getScrollbarWidth = () => {
  const container = document.createElement("div");
  container.style.visibility = "hidden";
  container.style.overflow = "scroll";
  const inner = document.createElement("div");

  container.appendChild(inner);
  document.body.appendChild(container);
  const scrollbarWidth = container.offsetWidth - inner.offsetWidth;
  document.body.removeChild(container);

  return scrollbarWidth;
};
