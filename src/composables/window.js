import { onMounted, onUnmounted } from 'vue'

export function useWindowEventListener(event, listener, options) {
  onMounted(() => {
    window.addEventListener(event, listener, options);
  });

  onUnmounted(() => {
    window.removeEventListener(event, listener);
  });
}
