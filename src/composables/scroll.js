import { IN_BROWSER, getScrollbarWidth, noScrollableParent } from '../util'

export const useScroll = ({
  overlay,
  content,
  contentActiveClass,
  fullscreen,
  contentFullscreenClass,
}) => {
  if (!IN_BROWSER) {
    return {
      disableScroll: () => {},
      enableScroll: () => {},
    }
  }

  let disabled = false
  let disableType

  const eventListener = (event) => {
    if (event.target === overlay.value
      || event.target === document.body
      || noScrollableParent(event, content.value)
    )
      event.preventDefault()
  }

  const scrollbarWidth = getScrollbarWidth()
  const zeroScrollBar = scrollbarWidth === 0

  const disableScroll = () => {
    if (disabled)
      return

    if (zeroScrollBar || fullscreen) {
      disableType = 'byOverflow'
      document.documentElement.classList.add('overflow-y-hidden')
    }
    else {
      disableType = 'byEvents'
      window.addEventListener('wheel', eventListener, {
        passive: false,
      })
    }

    disabled = true
  }

  const enableScroll = () => {
    if (!disabled)
      return

    if (disableType === 'byEvents') {
      window.removeEventListener('wheel', eventListener)
    }
    else if (disableType === 'byOverflow') {
      const activeContentElements = document.getElementsByClassName(contentActiveClass)
      const activeFullscreenContentElements = document.getElementsByClassName(contentFullscreenClass)

      if ((!zeroScrollBar && fullscreen && activeFullscreenContentElements.length === 1) || activeContentElements.length === 1)
        document.documentElement.classList.remove('overflow-y-hidden')
    }

    disabled = false
  }

  return {
    disableScroll,
    enableScroll,
  }
}
