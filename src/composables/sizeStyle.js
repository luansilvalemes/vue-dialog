import { computed } from 'vue'
import { convertToUnit } from '../util'

export const useSizeStyle = (props) => {
  const sizeStyles = computed(() => ({
    maxWidth:
      props.maxWidth === 'none'
        ? undefined
        : convertToUnit(props.maxWidth),

    width:
      props.width === 'auto'
        ? undefined
        : convertToUnit(props.width),

    height:
      props.height === 'auto'
        ? undefined
        : convertToUnit(props.height),
  }))

  return {
    sizeStyles,
  }
}