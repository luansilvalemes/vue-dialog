import { nextTick, ref, watch } from 'vue'

export const useLazyActivation = (baseState) => {
  const activatedOnce = ref(false)
  const active = ref(false)

  if (baseState.value) {
    activatedOnce.value = true
    nextTick(() => {
      active.value = true
    })
  }

  watch(baseState, (value) => {
    if (!activatedOnce.value) {
      activatedOnce.value = true
      nextTick(() => {
        active.value = value
      })

      return
    }

    active.value = value
  })

  return {
    activatedOnce,
    active,
  }
}
