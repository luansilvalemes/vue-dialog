import './scss/main.scss';

// standalone component
const GDialog = require('./components/GDialog.vue').default;
module.exports.GDialog = GDialog;

// using plugin
const {
  IDialog,
  DialogOnCloseEvent,
  IDialogItem,
} = require('./types/Plugin');
module.exports.IDialog = IDialog;
module.exports.DialogOnCloseEvent = DialogOnCloseEvent;
module.exports.IDialogItem = IDialogItem;

const GDialogRoot = require('./components/GDialogRoot.vue').default;
module.exports.GDialogRoot = GDialogRoot;

const { plugin, dialogInjectionKey } = require('./plugin');
module.exports.plugin = plugin;
module.exports.dialogInjectionKey = dialogInjectionKey;