import { reactive, shallowReactive } from 'vue';

const dialogs = shallowReactive([]);

export const errorLogger = {
  pluginIsNotInitialized() {
    console.error('The gitart-vue-dialog plugin is not initialized. Read how to solve: https://gitart-vue-dialog.gitart.org/guide/usage/plugin-usage.html#installation');
  },
};

export const dialogInjectionKey = Symbol('GDialog');

export const dialogInjectionFallback = {
  dialogs: [],
  addDialog: () => {
    errorLogger.pluginIsNotInitialized();
    return null;
  },
  removeDialog: () => {
    errorLogger.pluginIsNotInitialized();
  },
};

export const plugin = {
  install(app, options) {
    const defaultCloseDelay = options?.closeDelay ?? 500;
    const defaultProps = options?.props ?? {};

    const $dialog = {
      dialogs,

      addDialog({ component, props, id }, hooks) {
        const dialogId = id ?? Date.now() + Math.random();

        dialogs.push({
          component,
          id: dialogId,

          props: reactive({
            modelValue: true,
            ...defaultProps,
            ...props,
          }),

          onClose: hooks?.onClose,
        });

        return dialogId;
      },

      removeDialog(id, closeDelay) {
        const dialog = dialogs.find(d => d.id === id);

        if (!dialog || !dialog.props.modelValue) {
          return;
        }

        let canceled = false;
        const event = {
          id,
          cancel() {
            console.warn('Dialog closing canceled');
            canceled = true;
          },
          item: dialog,
        };

        if (dialog.onClose) {
          dialog.onClose(event);

          if (canceled) {
            return;
          }
        }

        dialog.props.modelValue = false;
        setTimeout(() => {
          dialogs.splice(dialogs.indexOf(dialog), 1);
        }, closeDelay ?? defaultCloseDelay);
      },
    };

    app.provide(dialogInjectionKey, $dialog);
    app.config.globalProperties.$dialog = $dialog;
  },
};
