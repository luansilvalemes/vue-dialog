import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

export default defineConfig({
  plugins: [],
  test: {
    global: true,
    environment: 'happy-dom',
  },
})
